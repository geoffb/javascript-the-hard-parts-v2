// http://csbin.io/callbacks

// Challenge 1
const addTwo = num => num + 2
//console.log(addTwo(3));
//console.log(addTwo(10));


// Challenge 2
const addS = string => string + 's'
//console.log(addS('pizza'));
//console.log(addS('bagel'));


// Challenge 3
function map(array, callback) {
	return array.map(item => callback(item))
}
//console.log(map([1, 2, 3], addTwo));


// Challenge 4
function forEach(array, callback) {
  // Online solution:
  // for (let i = 0; i < array.length; i++) {
  // 	console.log("call 2")
  //   callback(array[i]);
  // }
	array.forEach(item => {
		callback(item)
	})
}

//forEach([1, 2, 3], addTwo)
//console.log(result)

// see for yourself if your forEach works!


// Challenge 5
function mapWith(array, callback) {
	const result = []
  forEach(array, el => result.push(callback(el)))
  return result
}

//console.log(mapWith([1, 2, 3], addTwo))


// Challenge 6
function reduce(array, callback, initialValue) {
  let acc = initialValue
  array.map(item => {
    acc = callback(acc, item)
  })
  return acc
  
  // My fast solution:
  //return array.reduce(callback)
}

//console.log(reduce([4, 1, 3], (a, b) => a + b, 0))


// Challenge 7
function intersection(arrays) {
  // My solution with our custom reduce:
// 	const callback = (acc, curr) => {
//     // acc.map(item => {
//     // 	if (!curr.includes(item)) {
//     //   	acc = acc.filter(e => e !== item)
//     // 	}
//     // })
//     // return acc
    
//     // Other solution:
//     //return acc.filter(el => curr.includes(el))
// 	}
// 	return reduce(arrays, callback, arrays[0])
  
  //Solution with reduce:
  return arrays.reduce((acc, curr) => {
    return curr.filter(el => acc.includes(el))
  })
}

// console.log(intersection([[5, 10, 15, 20], [15, 88, 1, 5, 7], [1, 10, 15, 5, 20]]));
// should log: [5, 15]


// Challenge 8
function union(arrays) {
	return arrays.reduce((acc, curr) => {
    // const res = curr.filter(el => !acc.includes(el) && acc.push(el))
    // return acc
    const newNumbers = curr.filter(el => !acc.includes(el))
    return acc.concat(newNumbers)
  })
}

//console.log(union([[5, 10, 15], [15, 88, 1, 5, 7], [100, 15, 10, 1, 5]]));
// should log: [5, 10, 15, 88, 1, 7, 100]


// Challenge 9
function objOfMatches(array1, array2, callback) {
	const newObject = {}
  array1.map((elem, index) => {
    if (array2[index] === callback(elem)) {
      newObject[elem] = array2[index]
    }
	})
  return newObject
}

//console.log(objOfMatches(['hi', 'howdy', 'bye', 'later', 'hello'], ['HI', 'Howdy', 'BYE', 'LATER', 'hello'], function(str) { return str.toUpperCase(); }));
// should log: { hi: 'HI', bye: 'BYE', later: 'LATER' }


// Challenge 10
function multiMap(arrVals, arrCallbacks) {
  const newObject = {}
  arrVals.map((elem) => newObject[elem] = arrCallbacks.map((func) => func(elem)))
	return newObject
}

//console.log(multiMap(['catfood', 'glue', 'beer'], [function(str) { return str.toUpperCase(); }, function(str) { return str[0].toUpperCase() + str.slice(1).toLowerCase(); }, function(str) { return str + str; }]));
// should log: { catfood: ['CATFOOD', 'Catfood', 'catfoodcatfood'], glue: ['GLUE', 'Glue', 'glueglue'], beer: ['BEER', 'Beer', 'beerbeer'] }


// Challenge 11
function objectFilter(obj, callback) {
  const newObject = {}
	Object.entries(obj).map((elem) => {if (elem[1] === callback(elem[0])) newObject[elem[0]] = elem[1]})
  return newObject
}

// const cities = {
// London: 'LONDON',
// LA: 'Los Angeles',
// Paris: 'PARIS',
// };
// console.log(objectFilter(cities, city => city.toUpperCase())) // Should log { London: 'LONDON', Paris: 'PARIS'}


// Challenge 12
function majority(array, callback) {
	const odd = []
  const even = []
  array.map((elem) => {
    callback(elem) ? odd.push(elem) : even.push(elem)
  })
  return odd.length > even.length
}

// /*** Uncomment these to check your work! ***/
// const isOdd = function(num) { return num % 2 === 1; };
// console.log(majority([1, 2, 3, 4, 5], isOdd)); // should log: true
// console.log(majority([2, 3, 4, 5], isOdd)); // should log: false


// Challenge 13
function prioritize(array, callback) {
	const pass = []
  const fail = []
  array.map((elem) => {
    callback(elem) ? pass.push(elem) : fail.push(elem)
  })
  return pass.concat(fail)
}

// /*** Uncomment these to check your work! ***/
 // const startsWithS = function(str) { return str[0] === 's' || str[0] === 'S'; };
 // console.log(prioritize(['curb', 'rickandmorty', 'seinfeld', 'sunny', 'friends'], startsWithS)); 
// should log: ['seinfeld', 'sunny', 'curb', 'rickandmorty', 'friends']


// Challenge 14
function countBy(array, callback) {
	const res = {}
  array.map((elem) => {
    const callbackRes = callback(elem)
    Object.keys(res).includes(callbackRes) ? res[callbackRes] = res[callbackRes] + 1 : res[callbackRes] = 1 
  })
  return res
}

// /*** Uncomment these to check your work! ***/
// console.log(countBy([1, 2, 3, 4, 5], function(num) {
// if (num % 2 === 0) return 'even';
// else return 'odd';
// })); // should log: { odd: 3, even: 2 }


// Challenge 15
function groupBy(array, callback) {
	const res = {}
  array.map((elem) => {
    const callbackRes = callback(elem)
    Object.keys(res).includes(callbackRes.toString()) ? res[callbackRes].push(elem) : res[callbackRes] = [elem]
  })
  return res
}

// /*** Uncomment these to check your work! ***/
// const decimals = [1.3, 2.1, 2.4];
// const floored = function(num) { return Math.floor(num); };
// console.log(groupBy(decimals, floored)); // should log: { 1: [1.3], 2: [2.1, 2.4] }


// Challenge 16
function goodKeys(obj, callback) {
	return Object.entries(obj).map(e => callback(e[1]) ? e[0] : '').filter(e => e !== '')
}

// /*** Uncomment these to check your work! ***/
// const sunny = { mac: 'priest', dennis: 'calculating', charlie: 'birdlaw', dee: 'bird', frank: 'warthog' };
// const startsWithBird = function(str) { return str.slice(0, 4).toLowerCase() === 'bird'; };
// console.log(goodKeys(sunny, startsWithBird)); // should log: ['charlie', 'dee']


// Challenge 17
function commutative(func1, func2, value) {
	return func2(func1(value)) === func1(func2(value))
}

// /*** Uncomment these to check your work! ***/
// const multBy3 = n => n * 3;
// const divBy4 = n => n / 4;
// const subtract5 = n => n - 5;
// console.log(commutative(multBy3, divBy4, 11)); // should log: true
// console.log(commutative(multBy3, subtract5, 10)); // should log: false
// console.log(commutative(divBy4, subtract5, 48)); // should log: false


// Challenge 18
function objFilter(obj, callback) {
	const res = {}
  Object.entries(obj).map(e => callback(e[0]) === e[1] ? res[e[0]] = e[1] : '')
  return res
}

// /*** Uncomment these to check your work! ***/
// const startingObj = {};
// startingObj[6] = 3;
// startingObj[2] = 1;
// startingObj[12] = 4;
// const half = n => n / 2;
// console.log(objFilter(startingObj, half)); // should log: { 2: 1, 6: 3 }


// Challenge 19
function rating(arrOfFuncs, value) {
  let res = 100
	arrOfFuncs.map(e => !e(value) ? res = res - (100 / arrOfFuncs.length) : '')
  return res
}

// /*** Uncomment these to check your work! ***/
// const isEven = n => n % 2 === 0;
// const greaterThanFour = n => n > 4;
// const isSquare = n => Math.sqrt(n) % 1 === 0;
// const hasSix = n => n.toString().includes('6');
// const checks = [isEven, greaterThanFour, isSquare, hasSix];
// console.log(rating(checks, 64)); // should log: 100
// console.log(rating(checks, 66)); // should log: 75


// Challenge 20
function pipe(arrOfFuncs, value) {
  let tmp = value
	return arrOfFuncs.map(e => tmp = e(tmp))[arrOfFuncs.length-1]
}

// /*** Uncomment these to check your work! ***/
// const capitalize = str => str.toUpperCase();
// const addLowerCase = str => str + str.toLowerCase();
// const repeat = str => str + str;
// const capAddlowRepeat = [capitalize, addLowerCase, repeat];
// console.log(pipe(capAddlowRepeat, 'cat')); // should log: 'CATcatCATcat'


// Challenge 21
function highestFunc(objOfFuncs, subject) {
	const tmp = {}
	Object.entries(objOfFuncs).map(e => {
	tmp[e[0]] = e[1](subject)
	})
  
  const sortable = Object.fromEntries(
    Object.entries(tmp).sort(([,a],[,b]) => a-b)
	)

	const keys = Object.keys(sortable)
  return keys[keys.length-1]
}

/*** Uncomment these to check your work! ***/
// const groupOfFuncs = {};
// groupOfFuncs.double = n => n * 2;
// groupOfFuncs.addTen = n => n + 10;
// groupOfFuncs.inverse = n => n * -1;
// console.log(highestFunc(groupOfFuncs, 5)); // should log: 'addTen'
// console.log(highestFunc(groupOfFuncs, 11)); // should log: 'double'
// console.log(highestFunc(groupOfFuncs, -20)); // should log: 'inverse'


// Challenge 22
function combineOperations(startVal, arrOfFuncs) {
	arrOfFuncs.forEach(e => startVal = e(startVal))
  return startVal
}
function add100(num) {
  return num + 100;
}
function addTen(num) {
  return num + 10;
}
function divByFive(num) {
  return num / 5;
}
function multiplyByThree(num) {
  return num * 3;
}
function multiplyFive(num) {
  return num * 5;
}
// /*** Uncomment these to check your work! ***/
// console.log(combineOperations(0, [add100, divByFive, multiplyByThree])); // Should output 60 -->
// console.log(combineOperations(0, [divByFive, multiplyFive, addTen])); // Should output 10


// Challenge 23
function myFunc(array, callback) {
  return array.findIndex(e => callback(e))
}

const numbers = [2, 3, 6, 64, 10, 8, 12];
const evens = [2, 4, 6, 8, 10, 12, 64];

function isOddF(num) {
  return (num % 2 !== 0);
}

// /*** Uncomment these to check your work! ***/
// console.log(myFunc(numbers, isOddF)); // Output should be 1
// console.log(myFunc(evens, isOddF)); // Output should be -1


// Challenge 24
function myForEach(array, callback) {
	for (let i = 0;i < array.length; i++) {
    callback(array[i])
  }
}

let sum = 0;

function addToSum(num) {
  sum += num;
}

// /*** Uncomment these to check your work! ***/
const nums = [1, 2, 3];
myForEach(nums, addToSum);
console.log(sum); // Should output 6

