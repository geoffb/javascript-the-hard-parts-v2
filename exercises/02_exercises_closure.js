// Type JavaScript here and click "Run Code" or press Ctrl + s

// CHALLENGE 1
function createFunction() {
  const printHello = () => {
	  console.log('Hello, world!')
  }
  return printHello
}

// /*** Uncomment these to check your work! ***/
// const function1 = createFunction()
// function1() // => should console.log('hello');


// CHALLENGE 2
function createFunctionPrinter(input) {
	const printer = () => {
    console.log(input)
  }
  return printer
}

// /*** Uncomment these to check your work! ***/
// const printSample = createFunctionPrinter('sample')
// printSample() // => should console.log('sample');
// const printHello = createFunctionPrinter('hello')
// printHello() // => should console.log('hello');


// CHALLENGE 3
function outer() {
  let counter = 0; // this variable is outside incrementCounter's scope
  function incrementCounter () {
    counter ++;
    console.log('counter', counter);
  }
  return incrementCounter;
}

const willCounter = outer();
const jasCounter = outer();

// Uncomment each of these lines one by one.
// Before your do, guess what will be logged from each function call.

// /*** Uncomment these to check your work! ***/
// willCounter();
// willCounter();
// willCounter();

// jasCounter();
// willCounter();


function addByX(x) {
	const add = (y) => {
    return x + y
  }
  return add
}

// /*** Uncomment these to check your work! ***/
const addByTwo = addByX(2);
// addByTwo(1); // => should return 3
// addByTwo(2); // => should return 4
// addByTwo(3); // => should return 5

// const addByThree = addByX(3);
// addByThree(1); // => should return 4
// addByThree(2); // => should return 5

// const addByFour = addByX(4);
// addByFour(4); // => should return 8
// addByFour(5); // => should return 9


// CHALLENGE 4
function once(func) {
  let val = 0
	const inner = (x) => {
    if (val === 0) {
      val = func(x)
    }
    return val
  }
  return inner
}

// /*** Uncomment these to check your work! ***/
const onceFunc = once(addByTwo);
// console.log(onceFunc(4));  // => should log 6
// console.log(onceFunc(10));  // => should log 6
// console.log(onceFunc(9001));  // => should log 6


// CHALLENGE 5
function after(count, func) {
	let counter = 0
  const inner = (val) => {
    counter++
    if (counter >= count) func(val)
  }
  return inner
}

// // /*** Uncomment these to check your work! ***/
// const called = function(val) { console.log(val) };
// const afterCalled = after(3, called);
// afterCalled(1); // => nothing is printed
// afterCalled(2); // => nothing is printed
// afterCalled(3); // => '3' is printed
// afterCalled(4); // => '4' is printed
// afterCalled(5); // => '5' is printed


// CHALLENGE 6
function delay(func, wait, ...args) {
	const inner = () => {
    setTimeout(() => func(args), wait)
  }
  return inner
}

// const delayFunc = delay((val) => {val.forEach(e => console.log(e))}, 3000, 'hello', 'world')
// delayFunc()

// CHALLENGE 7
function rollCall(names) {
  const inner = () => {
    !names.length ? console.log('Everyone accounted for') : console.log(names.shift())
  }
  return inner
}

// /*** Uncomment these to check your work! ***/
const rollCaller = rollCall(['Victoria', 'Juan', 'Ruth'])
// rollCaller() // => should log 'Victoria'
// rollCaller() // => should log 'Juan'
// rollCaller() // => should log 'Ruth'
// rollCaller() // => should log 'Everyone accounted for'


// CHALLENGE 8
function saveOutput(func, magicWord) {
	const pastOperations = {}
  const inner = (val) => {
    if (val === magicWord) return pastOperations
    const res = func(val)
    pastOperations[val] = res
    return res
  }
  return inner
}

// /*** Uncomment these to check your work! ***/
// const multiplyBy2 = function(num) { return num * 2; };
// const multBy2AndLog = saveOutput(multiplyBy2, 'boo');
// console.log(multBy2AndLog(2)); // => should log 4
// console.log(multBy2AndLog(9)); // => should log 18
// console.log(multBy2AndLog('boo')); // => should log { 2: 4, 9: 18 }


// CHALLENGE 9
function cycleIterator(array) {
	let index = -1
  const inner = () => {
    index = index === -1 || index === array.length - 1 ? index = 0 : index + 1
    return array[index]
  }
  return inner
}

// /*** Uncomment these to check your work! ***/
const threeDayWeekend = ['Fri', 'Sat', 'Sun'];
// const getDay = cycleIterator(threeDayWeekend);
// console.log(getDay()); // => should log 'Fri'
// console.log(getDay()); // => should log 'Sat'
// console.log(getDay()); // => should log 'Sun'
// console.log(getDay()); // => should log 'Fri'


// CHALLENGE 10
function defineFirstArg(func, arg) {
	const inner = (x) => {
    return func(arg,x)
  }
  return inner
}

// /*** Uncomment these to check your work! ***/
// const subtract = function(big, small) { return big - small; };
// const subFrom20 = defineFirstArg(subtract, 20);
// console.log(subFrom20(5)); // => should log 15


// CHALLENGE 11
function dateStamp(func) {
	const inner = (...args) => {
    return {
      date: Date.now(),
      output: func(args)
    }
  }
  return inner
}

// /*** Uncomment these to check your work! ***/
// const stampedMultBy2 = dateStamp(n => n * 2);
// console.log(stampedMultBy2(4)); // => should log { date: (today's date), output: 8 }
// console.log(stampedMultBy2(6)); // => should log { date: (today's date), output: 12 }


// CHALLENGE 12
function censor() {
  const text = {}
	const inner = (arg1, arg2) => {
    if (!arg2) {
      let str = arg1
      Object.entries(text).map(e => str = str.replace(e[0], e[1]))
      return str
    }
    text[arg1] = arg2
  }
  return inner
}

// /*** Uncomment these to check your work! ***/
const changeScene = censor();
// changeScene('dogs', 'cats');
// changeScene('quick', 'slow');
// console.log(changeScene('The quick, brown fox jumps over the lazy dogs.')); // => should log 'The slow, brown fox jumps over the lazy cats.'


// CHALLENGE 13
function createSecretHolder(secret) {
	let privateSecret = secret
  return {
    setSecret: (newSecret) => privateSecret = newSecret,
  	getSecret: () => {return privateSecret}
	}
}

// /*** Uncomment these to check your work! ***/
// const obj = createSecretHolder(5)
// console.log(obj.getSecret()) // => returns 5
// obj.setSecret(2)
// console.log(obj.getSecret()) // => returns 2


// CHALLENGE 14
function callTimes() {
	let nbCalls = 0
  const inner = () => {
    nbCalls++
    console.log(nbCalls)
  }
  return inner
}

// /*** Uncomment these to check your work! ***/
// let myNewFunc1 = callTimes();
// let myNewFunc2 = callTimes();
// myNewFunc1(); // => 1
// myNewFunc1(); // => 2
// myNewFunc2(); // => 1
// myNewFunc2(); // => 2


// CHALLENGE 15
function russianRoulette(num) {
	let bang = num
  const inner = () => {
    bang--
    return bang === 0 ? 'bang!' : bang > 0 ? 'click..' : 'Reload to play again'
  }
  return inner
}

// /*** Uncomment these to check your work! ***/
// const play = russianRoulette(3);
// console.log(play()); // => should log 'click'
// console.log(play()); // => should log 'click'
// console.log(play()); // => should log 'bang'
// console.log(play()); // => should log 'reload to play again'
// console.log(play()); // => should log 'reload to play again'


// CHALLENGE 16
function average() {
	const numbers = []
  const inner = (num) => {
    if (num) numbers.push(num)
    if (numbers.length === 0) return 0
    return numbers.reduce((res, curr) => res + curr) / numbers.length
  }
  return inner
}

// /*** Uncomment these to check your work! ***/
// const avgSoFar = average();
// console.log(avgSoFar()); // => should log 0
// console.log(avgSoFar(4)); // => should log 4
// console.log(avgSoFar(8)); // => should log 6
// console.log(avgSoFar()); // => should log 6
// console.log(avgSoFar(12)); // => should log 8
// console.log(avgSoFar()); // => should log 8


// CHALLENGE 17
function makeFuncTester(arrOfTests) {
  const inner = (callback) => {
    const res = arrOfTests.map(e => callback(e[0]) === e[1])
    return !res.includes(false)
  }
  return inner
}

// /*** Uncomment these to check your work! ***/
// const capLastTestCases = [];
// capLastTestCases.push(['hello', 'hellO']);
// capLastTestCases.push(['goodbye', 'goodbyE']);
// capLastTestCases.push(['howdy', 'howdY']);
// const shouldCapitalizeLast = makeFuncTester(capLastTestCases);
// const capLastAttempt1 = str => str.toUpperCase();
// const capLastAttempt2 = str => str.slice(0, -1) + str.slice(-1).toUpperCase();
// console.log(shouldCapitalizeLast(capLastAttempt1)); // => should log false
// console.log(shouldCapitalizeLast(capLastAttempt2)); // => should log true


// CHALLENGE 18
// CHALLENGE 18
function makeHistory(limit) {
	const history = []
  const inner = (action) => {
    if (action === 'undo') {
      const resPop = history.pop()
      return resPop ? `${resPop} undone` : 'nothing to undo'
    }
    if (history.length === limit) history.shift()
    history.push(action)
    return `${action} done`
  }
  return inner
}

// /*** Uncomment these to check your work! ***/
// const myActions = makeHistory(2);
// console.log(myActions('jump')); // => should log 'jump done'
// console.log(myActions('undo')); // => should log 'jump undone'
// console.log(myActions('walk')); // => should log 'walk done'
// console.log(myActions('code')); // => should log 'code done'
// console.log(myActions('pose')); // => should log 'pose done'
// console.log(myActions('undo')); // => should log 'pose undone'
// console.log(myActions('undo')); // => should log 'code undone'
// console.log(myActions('undo')); // => should log 'nothing to undo'


// CHALLENGE 19
function blackjack(array) {
  const inner = (card1, card2) => {
		let sum = 0
  	let lost = false
    return () => {
      if (lost) return 'You are done!'
      if (sum === 0) {
        sum = card1 + card2
        return sum
    	}
      sum += array.shift()
      if (sum >= 21) {
        lost = true
        return 'Bust!'
      } 
     	return sum
    }
  }
  return inner
}

// /*** Uncomment these to check your work! ***/

// /*** DEALER ***/
const deal = blackjack([2, 6, 1, 7, 11, 4, 6, 3, 9, 8, 9, 3, 10, 4, 5, 3, 7, 4, 9, 6, 10, 11]);

// /*** PLAYER 1 ***/
// const i_like_to_live_dangerously = deal(4, 5);
// console.log(i_like_to_live_dangerously()); // => should log 9
// console.log(i_like_to_live_dangerously()); // => should log 11
// console.log(i_like_to_live_dangerously()); // => should log 17
// console.log(i_like_to_live_dangerously()); // => should log 18
// console.log(i_like_to_live_dangerously()); // => should log 'bust'
// console.log(i_like_to_live_dangerously()); // => should log 'you are done!'
// console.log(i_like_to_live_dangerously()); // => should log 'you are done!'

// /*** BELOW LINES ARE FOR THE BONUS ***/

// /*** PLAYER 2 ***/
// const i_TOO_like_to_live_dangerously = deal(2, 2);
// console.log(i_TOO_like_to_live_dangerously()); // => should log 4
// console.log(i_TOO_like_to_live_dangerously()); // => should log 15
// console.log(i_TOO_like_to_live_dangerously()); // => should log 19
// console.log(i_TOO_like_to_live_dangerously()); // => should log 'bust'
// console.log(i_TOO_like_to_live_dangerously()); // => should log 'you are done!
// console.log(i_TOO_like_to_live_dangerously()); // => should log 'you are done!

// /*** PLAYER 3 ***/
// const i_ALSO_like_to_live_dangerously = deal(3, 7);
// console.log(i_ALSO_like_to_live_dangerously()); // => should log 10
// console.log(i_ALSO_like_to_live_dangerously()); // => should log 13
// console.log(i_ALSO_like_to_live_dangerously()); // => should log 'bust'
// console.log(i_ALSO_like_to_live_dangerously()); // => should log 'you are done!



