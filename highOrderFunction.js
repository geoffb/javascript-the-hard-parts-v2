const highOrderFunction = (array, instructions) => {
  const output = []

  for (let i = 0; i < array.length; i++) {
    output.push(instructions(array[i]))
  }

  return output
}

const highOrderFunctionES6 = (array, instructions) => array.map(item => instructions(item))

const multiplyByTwo = input => input * 2
const result = highOrderFunction([1, 2, 3], multiplyByTwo)
const resultES6 = highOrderFunctionES6([1, 2, 3], multiplyByTwo)

console.log(result, resultES6)
