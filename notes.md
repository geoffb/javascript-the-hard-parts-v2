
# Frontend masters - Javascript: The hard parts v2
https://frontendmasters.com/courses/javascript-hard-parts-v2/
https://static.frontendmasters.com/resources/2019-09-18-javascript-hard-parts-v2/javascript-hard-parts-v2.pdf

---------------------------------------------------

## Call Stack

Stack in which the function are executed in order.  
'global' is always part of the call stack

## Execution context
	
A context in which the function runs

For ex:

	const multiplyBy2 = (input) => {return input * 2}
	const output = multiplyBy2(3)

Runs in order:

- **Global memory**  
multiplyBy2: function  
output

- **Call Stack**  
output = multiplyBy2(3)

- **Execution context of multiplyBy2(3)**

|                  | Local memory                                |
|------------------|---------------------------------------------|
|                  | value 3 goes first and is assigned to input |
|                  | input: 3                                    |
| return input * 2 |                                             |
| 3*2              |                                             |
| 6                |                                             |

Then 6 is placed inside output

## High order function

	const highOrderFunction = (array, instructions) => {
	  const output = []

	  for (let i = 0; i < array.length; i++) {
	    output.push(instructions(array[i]))
	  }

	  return output
	}

	const highOrderFunctionES6 = (array, instructions) => {
	  return array.map(item => instructions(item))
	}

	const multiplyByTwo = (input) => {return input * 2}
	const result = highOrderFunction([1, 2, 3], multiplyByTwo)
	const resultES6 = highOrderFunctionES6([1, 2, 3], multiplyByTwo)

	console.log(result, resultES6)

But we can refactor the multiplyByTwo function:

	const multiplyByTwo = input => input * 2

And also the ES6 function

	const highOrderFunctionES6 = (array, instructions) => array.map(item => instructions(item))

### Exercises 

Exercises: http://csbin.io/callbacks

Important pieces:

**map**

**forEach**

**reduce**

Syntax:
```
arr.reduce(callback( accumulator, currentValue, [, index[, array]] )[, initialValue])
```

The reduce() method executes a reducer function (that you provide) on each element of the array, resulting in single output value.

The reducer function takes four arguments:

- Accumulator (acc)
- Current Value (cur)
- Current Index (idx)
- Source Array (src)

Your reducer function's returned value is assigned to the accumulator, whose value is remembered across each iteration throughout the array, and ultimately becomes the final, single resulting value.

initialValue (Optional):

A value to use as the first argument to the first call of the callback. If no initialValue is supplied, the first element in the array will be used as the initial accumulator value and skipped as currentValue. Calling reduce() on an empty array without an initialValue will throw a TypeError.

Ex: ```array.reduce(reducer, 5)```

```
const array1 = [1, 2, 3, 4];
const reducer = (accumulator, currentValue) => accumulator + currentValue;

// 1 + 2 + 3 + 4
console.log(array1.reduce(reducer));
// expected output: 10

// 5 + 1 + 2 + 3 + 4
console.log(array1.reduce(reducer, 5));
// expected output: 15

But can also be written like:
console.log(
  array1.reduce((acc, curr) => {
    return acc + curr
  })
)
// expected output: 10

```

**concat**

The concat() method is used to merge two or more arrays. This method does not change the existing arrays, but instead returns a new array.

```
const array1 = ['a', 'b', 'c'];
const array2 = ['d', 'e', 'f'];
const array3 = array1.concat(array2);

console.log(array3);
// expected output: Array ["a", "b", "c", "d", "e", "f"]
```

**find**

The find() method returns the value of the first element in the provided array that satisfies the provided testing function.

- If you need the index of the found element in the array, use findIndex().
- If you need to find the index of a value, use Array.prototype.indexOf(). (It’s similar to findIndex(), but checks each element for equality with the value instead of using a testing function.)
- If you need to find if a value exists in an array, use Array.prototype.includes(). Again, it checks each element for equality with the value instead of using a testing function.
- If you need to find if any element satisfies the provided testing function, use Array.prototype.some().

```
const array1 = [5, 12, 8, 130, 44];

const found = array1.find(element => element > 10);

console.log(found);
// expected output: 12
```

**includes**

array.includes(el) => boolean

**filter**

The filter() method creates a new array with all elements that pass the test implemented by the provided function.

const result = words.filter(word => word.length > 6);

array1.filter(el => el === 3) => array with elements that fit the filter

**sort**
The sort() method sorts the elements of an array in place and returns the sorted array. The default sort order is ascending, built upon converting the elements into strings, then comparing their sequences of UTF-16 code units values.

Syntax: arr.sort([compareFunction])

compareFunction [Optional]:
Specifies a function that defines the sort order. If omitted, the array elements are converted to strings, then sorted according to each character's Unicode code point value.

```
const months = ['March', 'Jan', 'Feb', 'Dec'];
months.sort();
console.log(months);
// expected output: Array ["Dec", "Feb", "Jan", "March"]
```

If compareFunction is supplied, all non-undefined array elements are sorted according to the return value of the compare function (all undefined elements are sorted to the end of the array, with no call to compareFunction). If a and b are two elements being compared, then:

- If compareFunction(a, b) returns **less than 0**, sort a to an index lower than b (i.e. a comes first).
- If compareFunction(a, b) **returns 0**, leave a and b unchanged with respect to each other, but sorted with respect to all different elements. Note: the ECMAscript standard does not guarantee this behavior, thus, not all browsers (e.g. Mozilla versions dating back to at least 2003) respect this.
- If compareFunction(a, b) returns **greater than 0**, sort b to an index lower than a (i.e. b comes first).
- compareFunction(a, b) must always return the same value when given a specific pair of elements a and b as its two arguments. If inconsistent results are returned, then the sort order is undefined.

**pop**
The pop() method removes the last element from an array and returns that element. This method changes the length of the array.

```
const plants = ['broccoli', 'cauliflower', 'cabbage', 'kale', 'tomato'];

console.log(plants.pop());
// expected output: "tomato"
```

**shift**
The shift() method removes the first element from an array and returns that removed element. This method changes the length of the array.
```
const array1 = [1, 2, 3];

const firstElement = array1.shift();

console.log(array1);
// expected output: Array [2, 3]

console.log(firstElement);
// expected output: 1
```

**splice**
The [splice()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice) method changes the contents of an array by removing or replacing existing elements and/or adding new elements in place.
```
const months = ['Jan', 'March', 'April', 'June'];
months.splice(1, 0, 'Feb');
// inserts at index 1
console.log(months);
// expected output: Array ["Jan", "Feb", "March", "April", "June"]

months.splice(4, 1, 'May');
// replaces 1 element at index 4
console.log(months);
// expected output: Array ["Jan", "Feb", "March", "April", "May"]
```
Returns:
- An array containing the deleted elements.
- If only one element is removed, an array of one element is returned.
- If no elements are removed, an empty array is returned.


## Closure

- Enables pro-level functions like 'once' and 'memoize'
- Many design pattern uses closure
- Build iterators

For example, our multiplyByTwo is brand new every time. It does not remember that it has been ran before.
New local memory everytime.

### Function with memories

- When our function gets called, we create a live store of data (local memory/variable env/state) fot that function exectuion context.

Functions can be returned from other functions in javascript
A function is a value

	function createFunction() {
		const multiplyBy2 = (num) => {
			return num*2
		}

		return multiplyBy2
	}

	const generatedFunc = createFunction()
	const result = generatedFunc(3) //6

|                  														| Global memory                               |
|------------------														|---------------------------------------------|
|                  														| createFunction: f                           |
|                  														| generatedFunc:                              |
| Createds a brand new execution context: 		|                                             |
| generatedFunc = createFunction              |                                             |
|                 [cf table below]						|                                             |


<br/><br/>
generatedFunc = createFunction()

|                  | Local memory                                |
|------------------|---------------------------------------------|
|                  | multiplyBy2: f                              |
|                  |                                       			 |
|(first thing)     |                                             |
|return multiplyBy2|                                             |
|(go look inside of local memory to find it)|                    |
<br/><br/>
The function definition gets returned without its label into generatedFunc.
The exucution context gets deleted.

Then it goes back to the first execution context.
To define a constant result equals to generatedFunc(3).
GeneratedFunc is the result of the one time running of createFunction.

Then it creates a new execution context:
<br/><br/>
result = generatedFunc(3)
|                  | Local memory                                |
|------------------|---------------------------------------------|
|                  | num: 3                                      |
| num * 2          |                                             |
| 3*2              |                                             |
| 6                |                                             |
<br/><br/>

Why did we do this in the first place?
Because it gets the most powerful thing javascript gives: memory

### Nested function scope

Where you define your functions determines what data it has access to when you call it.

### Retaining function memory
```
function outer (){
 let counter = 0;
 function incrementCounter (){ counter ++; }
 return incrementCounter;
}

const myNewFunction = outer();
myNewFunction();
myNewFunction();
```
The result of the increment counter in myNewFunction brought with it all the local memory and surrounding data. (Backpack!)
It pulls just the data it needs.

### Namings

Local memory = Variable environment
Backpack data = Persistant lexically scope referenced data PLSRD - javascript has a lexical scope
Closure = backpack and/or concept

### Once and memoize

#### Once
Is a closure function called only once. Put a counter in it and increment the counter when the function is used the first time. It will not be possible to run it again.

#### Memoize

Standard computer science concept.
Imagine we have a function to look up prime numbers. If I want the 1270001th prime number, I will go through the loop once and find it.
But if I ask again for the number we don't want to run it again, we will store the prime numbers (ex: object with key=nth, value=number).
The solution will be there right away.

Memoization = giving that function previous memories of their previous input / output combinations.

In depth explaination:
https://scotch.io/tutorials/understanding-memoization-in-javascript
Memoization is an optimization technique that speeds up applications by storing the results of expensive function calls and returning the cached result when the same inputs occur again.

### Lexical scoping
[Source](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)

Consider the following example code:
```
function init() {
  var name = 'Mozilla'; // name is a local variable created by init
  function displayName() { // displayName() is the inner function, a closure
    alert(name); // use variable declared in the parent function
  }
  displayName();
}
init();
```
init() creates a local variable called name and a function called displayName(). The displayName() function is an inner function that is defined inside init() and is available only within the body of the init() function. Note that the displayName() function has no local variables of its own. However, since inner functions have access to the variables of outer functions, displayName() can access the variable name declared in the parent function, init().

Run the code using this JSFiddle link and notice that the alert() statement within the displayName() function successfully displays the value of the name variable, which is declared in its parent function. **This is an example of lexical scoping, which describes how a parser resolves variable names when functions are nested.** The word lexical refers to the fact that lexical scoping uses the location where a variable is declared within the source code to determine where that variable is available. Nested functions have access to variables declared in their outer scope. 

### Iterators and generators

Which use lexical scoping and closure.
The function will remember the result of the last time the function ran. Or where it stopped.
Everything is saved to the bagpack.

### Closure Exercises

http://csbin.io/closures

```
function saveOutput(func, magicWord) {
	const pastOperations = {}
  const inner = (val) => {
    if (val === magicWord) return pastOperations
    const res = func(val)
    pastOperations[val] = res
    return res
  }
  return inner
}

// /*** Uncomment these to check your work! ***/
const multiplyBy2 = function(num) { return num * 2; };
const multBy2AndLog = saveOutput(multiplyBy2, 'boo');
console.log(multBy2AndLog(2)); // => should log 4
console.log(multBy2AndLog(9)); // => should log 18
console.log(multBy2AndLog('boo')); // => should log { 2: 4, 9: 18 }
```

Can even have private properties with setters and getters.
There is no way to read the privateSecret directly from the obj object. 
The method is to return it directly in the return object or create a getter.
```
// CHALLENGE 13
function createSecretHolder(secret) {
	let privateSecret = secret
  return {
    setSecret: (newSecret) => privateSecret = newSecret,
  	getSecret: () => {return privateSecret}
	}
}

// /*** Uncomment these to check your work! ***/
const obj = createSecretHolder(5)
console.log(obj.getSecret()) // => returns 5
obj.setSecret(2)
console.log(obj.getSecret()) // => returns 2
```

## Async, Promises and the Event Loop

JS is a synchronous langauge: if you go inside a function, you can move until that function is done.

### Callback queue

Event Loop:
- Is the call stack empty?
- If there are synchronous code to run, then run it
- Is the call stack empty?
- yes
- Is there anything in the callback queue
- yes
- Take the first one and run it
- Is the callstack empty?
- yes
- ...

This is why `printHello ` will never run until there is some synchronous code to run first. It could take ages before the 0ms function is ran.
```
function printHello(){
 console.log("Hello");
}
setTimeout(printHello,0);
console.log("Me first!");
```

### Exerices
http://csbin.io/async
https://github.com/CodesmithLLC/cs-bin-solutions/blob/master/async.js

### Promise
Take this example:
```
function display(data){console.log(data)}
function printHello(){console.log("Hello");}
function blockFor300ms(){/* blocks js thread for 300ms }

setTimeout(printHello, 0);
const futureData = fetch('https://twitter.com/will/tweets/1')
futureData.then(display)

blockFor300ms()
console.log("Me first!");
```

In order: 
- Me first will be logged to the console
- Then the display function will run
- And finally the Hello from the Callback stack will run

Why? Because the Promise response will go to another queue and this queue will have priority over the callback queue: Microtask queue.

## Classes and prototypes (OOP)

### Functions stores
```
function userCreator(name, score) {
	const newUser = {}; // Same as Object.create(null)
	newUser.name = name;
	newUser.score = score;
	newUser.increment = function() {
		newUser.score++;
	};
	return newUser;
};
const user1 = userCreator("Will", 3);
const user2 = userC
```

Problem: Each time we create a new user we make space in our computer's
memory for all our data and functions. But our functions are just copies.

Solution:

```
function userCreator (name, score) {
	const newUser = Object.create(userFunctionStore);
	newUser.name = name;
	newUser.score = score;
	return newUser;
};
const userFunctionStore = {
	increment: function(){this.score++;},
	login: function(){console.log("Logged in");}
};
const user1 = userCreator("Will", 3);
const user2 = userCreator("Tim", 5);
user1.increment();
```

Store the increment function in just one object and have the interpreter, if it
doesn't find the function on user1, look up to that object to check if it's there.

Object.create creates an empty object.
But it has a hiddent property: `__proto__` and it has stored a link to `userFunctionStore`.
Not to be confused to prototype.
Not as hidden as the lexical scope property. You can see it in the devtool.

Now `increment: function(){this.score++;},`:
It has an implicit parameter, this. This will reference whatever is on the left-handside of that method call: `user1.increment();`, here user1.

Then if we call `user1.hasOwnProperty('score')`, where does this method comes from?

All objects have a` __proto__` property by default which defaults to linking to a big
object - `Object.prototype` full of (somewhat) useful functions.
We get access to it via userFunctionStore’s `__proto__` property - the chain.

Here, as we know, this refers to user1:
```
const userFunctionStore = {
	increment: function() {
		this.score++;
	}
};
user1.increment(); 
```

But here, it doesn't work anymore. This is assigned to the global memory. It will not work.
```
const userFunctionStore = {
	increment: function() {
		function add1(){ this.score++; }
		add1()
	}
};
```

There was this old way that = this and then that.score++.

Or we can pass this as a parameter.

### Arrow functions and this

In arrow functions, `this` is lexically scopped. So this works:
```
const userFunctionStore = {
	increment: function(){
		const add = () => {
			this.score++;
		}
		add()
 },
};
```
Now our inner function `add` gets its this set by where it
was saved - it’s a ‘lexically scoped this

### `new` Keyword

From this:
```
function userCreator (name, score) {
	const newUser = Object.create(userFunctionStore);
	newUser.name = name;
	newUser.score = score;
	return newUser;
};
const user1 = userCreator("Will", 3);
```
To:
```
function userCreator (name, score) {
	this.name = name;
	this.score = score;
};
const user1 = new userCreator("Will", 3);
```

The `new` keywork does two things:
- create a new object
- return the new object

But now we don't have the link to the userFunctionStore with our methods.
We can directly attached:
```
userCreator.prototype.increment = function(){
	this.score++;
}
```

We could use the fact that all functions have a default property `prototype` on their object version, (itself an object) - to replace our `functionStore` object

```
function multiplyBy2(num){
	return num*2
}

multiplyBy2.stored = 5
multiplyBy2(3) // 6

multiplyBy2.stored // 5
multiplyBy2.prototype // {}
```

So now when we do this:
```
const user1 = new userCreator("Will", 3);
```
It puts in local memory:
- name: 'will'
- score: 3
- this: which is a label for the object {name: 'will', score: 3} with a hidden property `__proto__` which links to what's inside prototype.

### Class

The nicest method on how to solve this problem is to create a class
```
class UserCreator {
	constructor (name, score){
		this.name = name;
		this.score = score;
	}
	increment (){ this.score++; }
	login (){ console.log("login"); }
}
const user1 = new UserCreator("Eva", 9);
user1.increment();
```
It works exactly the same as before under the hood: the function object combo:
- A function userCreator (set with the constructor)
- An object containing prototype which itself contains the methods

The method increment is passed like this `UserCreator.prototype.increment`. Nothing has changed.
